# JavaShell

A simple wrapper script to execute java files as you would execute shell scripts

## Why?
With Java 11 it is possible to directly run Java files without compling them first.
With Java 11 you can do simply:

    java MyClass.java

Instead of

    javac MyClass.java
    java MyClass

which make Java a bit more "scripty".
JavaShell goes a step further and gives you the ability to execute Java files directly.

## Installation

Copy the file "jvsh" to the directory
    
    sudo cp jvsh /usr/bin/
    sudo chmod +x /usr/bin/jvsh

## Usage
Create your JavaShell file named
myFirstJavaShell with below content

    #! /usr/bin/jvsh
    
    // Now the actual java part
    import java.util.Arrays;
    
    public final class AnyClassName {
    	pubblic static void main(String[] args) {
    		System.out.println(
    			"Here are your arguments: " +
    			Arrays.asList(args)
    		);
    	}
    }

The first line stell the shell to use /usr/bin/jvsh to interpret the file. The rest is your java file to execute

### Run

    chmod +x myFirstJavaShell
    ./myFirstJavaShell Arg1 Arg2 Arg3

Will output:

    Here are your arguments: [Arg1, Arg2, Arg3]

Note: The example script just prints out the toString() from the Arrays.asList() result. Surely you can do a more nicer output

### Dependencies
If your script has some dependencies, you have give them in the script header. For each dependency add a line to the header:

    // dependency : org.apache.commons:commons-collections4:4.3

The maven dependency notation

    group:artifact:version

is used.

## How it works
JavaShell will first copy the script without the first line to a directory in /tmp/jvsh.
After that it will scan the dependencies, and will resolve them.
Then the script in /tmp/jvsh will be executed with all the needed libs added to the classpath.

After execution the script file in /tmp/jvsh and the libraries will be deleted.

## Limitations
Line comments beginning with

    // dependency : 

should be avoided. Since it JavaShell will try to resolve it line as dependency